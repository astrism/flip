var GRID_SIZE = 12;
var gridTimer;
var gridActive = true;

function initGrid()
{
	for(var i = GRID_SIZE; i > 0; i--)
	{
		$("#grid").prepend("<li id=grid" + i + " onclick='gridClick(" + i + ")'></li>");
		var gridItem = $("#grid" + i);
		gridItem.data("itemId", data[i - 1]);
		gridItem.data("gridId", i);
	}
}

function gridClick(id)
{
	var gridItem = $("#grid" + id);
	var itemId = gridItem.data("itemId");
	var gridId = gridItem.data("gridId");
	
	if(gridActive == false || currentItem != null && gridId == currentItem.data("gridId") || gridItem.hasClass("winner"))
		return;
	
	if(turn == 0)
	{		
		turn++;
		currentItem = gridItem;
		activateGridItem(gridItem, itemId);
	} else {
		turn--;
		activateGridItem(gridItem, itemId);
		if(itemId != currentItem.data("itemId")) //lose
		{
			updateScore(score);
			deactivateGrid();
			currentItem.removeClass("selected");
			currentItem.addClass("loser");
			gridItem.addClass("loser");
			gridTimer = setTimeout(function() {
				currentItem.removeClass("loser");
				gridItem.removeClass("loser");
				deactivateGridItem(currentItem);
				deactivateGridItem(gridItem);
				activateGrid();
			}, 1000);
		} else { //win	
			updateScore(score + 2);
			completes += 2;
			currentItem.addClass("winner");
			gridItem.addClass("winner");
		}
	}
}

function activateGridItem(gridItem, itemId)
{
	gridItem.html(itemId);
	gridItem.addClass('selected');
}

function deactivateGridItem(gridItem)
{
	gridItem.html("");
}

function activateGrid()
{
	gridActive = true;
}

function deactivateGrid()
{
	gridActive = false;
}
function clearGrid()
{
	currentItem = null;
	clearTimeout(gridTimer);
	gridTimer = null;
	$('.winner').html('');
	$('.loser').html('');
	$('.selected').html('');
	$('.winner').removeClass("winner");
	$('.loser').removeClass("loser");
	$('.selected').removeClass("selected");
}